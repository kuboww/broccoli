$(document).ready(function () {
//form styler
    $('input, select').styler({});
//slider
    $('.slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span></span></div>',
        prevArrow: '<div class="slick-arrow-left"><span></span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        // autoplay:true,

        responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 2

              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 1

              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]

    });
// appending

    $(window).on('load', function () {
        // if ($(window).width() >= 1200) {
            $('.jsAppendLeft').addClass("a-hidden").viewportChecker({
                classToAdd: 'animated slideInLeft',
                classToRemove: 'a-hidden',
                offset: 150
            });
            $('.jsAppendRight').addClass("a-hidden").viewportChecker({
                classToAdd: 'animated slideInRight',
                classToRemove: 'a-hidden',
                offset: 150
            });
            $('.jsAppendUp').addClass("a-hidden").viewportChecker({
                classToAdd: 'animated slideInUp',
                classToRemove: 'a-hidden',
                offset: 150
            });
        // }
    });


    $('.jsAppendRight').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $(this).removeClass('animated slideInRight full-visible a-hidden');

        if ($(this).hasClass('cssMove')) {
            $(this).addClass('shake shake-constant shake-slow');
        }
    });
    $('.jsAppendLeft').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $(this).removeClass('animated slideInLeft full-visible a-hidden');

        if ($(this).hasClass('cssMove')) {
            $(this).addClass('shake shake-constant shake-slow');
        }
    });

//smoothscroll
    $(document).on("scroll", onScroll);
    var ancor = $('#nav a, .scrolTosec');
    $(function () {
        ancor.click(function () {
            $('#nav').removeClass('open');
            $('#hamburger').removeClass('open');
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                && location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top - 125 //offsets for fixed header
                    }, 1000);
                    return false;
                }
            }
        });
        //Executed on page load with URL containing an anchor tag.
        if ($(location.href.split("#")[1])) {
            var target = $('#' + location.href.split("#")[1]);
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 125 //offset height of header here too.
                }, 1000);
                return false;
            }
        }
    });


    function onScroll(event) {
        var scrollPos = $(document).scrollTop();
        ancor.each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));
            if (refElement.position().top - 500 <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                ancor.removeClass("active");
                currLink.addClass("active");
            }
            else {
                currLink.removeClass("active");
            }
        });
    }

    $(window).on('load resize', function () {
        var header = $("header"),
            winWidth =$(window).width();
        if (winWidth > 1199) {
            $(window).scroll(function () {
               var scroll = $(window).scrollTop();
                if (scroll >= 500) {
                    header.addClass("fixed animated slideInDown");
                } else {
                    header.removeClass("fixed animated slideInDown");
                }
            });
        }
        else {
            header.removeClass("fixed animated slideInDown");
        }
    });

    $('.modal-content__table-butn .butn').on('click', function () {
        $(this).parents('.modal').modal('hide');
    });
// floating placeholder
    $(".modal-input, textarea").each(function (e) {
        $(this).wrap('<fieldset></fieldset>');
        var tag = $(this).attr("placeholder");
        //var tag= $(this).data("tag");
        $(this).attr("placeholder", "");
        $(this).after('<label for="name">' + tag + '</label>');
    });

    $('.modal-input, textarea').on('blur', function () {
        if (!$(this).val() == "") {
            $(this).next().addClass('stay');
        } else {
            $(this).next().removeClass('stay');
        }
    });
// phone mask
    $(function () {
        $('[name="phone"]').mask("+38-(000)-000-00-00", {
            clearIfNotMatch: true,

        });
        $('[name="phone"]').focus(function (e) {
            if ($('[name="tel"]').val().length == 0) {
                $(this).val('+38-(');
            }
        })
    });

// calc


    $('#jsCalc').click(function () {
        validate = 1;
        $('.jq-number .jsNumberOnly').each(function () {
            if ($(this).val() == '') {
                validate = 0;
                $(this).focus();
                $(this).addClass('red_input');
                $(this).keyup(function () {
                    $(this).removeClass('red_input');
                });
            }
            else {
                $(this).removeClass('red_input');
            }
        });
        if (validate == 1) {
                weight = $('#weight').val();
                height = $('#height').val();
                age = $('#age').val();
                sex = $('#sex').find('select').val() ;
                goal = $('#goal').find('select').val();
                activity = $('#activity').find('select').val();
                sumResult = parseInt(((((weight * 9.99 + height * 6.25 - age * 4.92) + 1 * sex) * goal) * activity), 10);

                $('#jsVal').text(sumResult);
            $('#result').modal('show');

        }
        else {}
    });

// //hamburger
    $('#hamburger').click(function(){
        $(this).toggleClass('open');
        $('#nav').toggleClass('open');


    });
//
//     $('main').click(function(){
//         $('#hamburger').removeClass('open');
//         $('#nav').removeClass('in');
//     });
//

//
//
// // autoheight textarea
//     (function($)
// {
//     /**
//      * Auto-growing textareas; technique ripped from Facebook
//      *
//      *
//      * http://github.com/jaz303/jquery-grab-bag/tree/master/javascripts/jquery.autogrow-textarea.js
//      */
//     $.fn.autogrow = function(options)
//     {
//         return this.filter('textarea').each(function()
//         {
//             var self         = this;
//             var $self        = $(self);
//             var minHeight    = $self.height();
//             var noFlickerPad = $self.hasClass('autogrow-short') ? 0 : parseInt($self.css('lineHeight')) || 0;
//             var settings = $.extend({
//                 preGrowCallback: null,
//                 postGrowCallback: null
//               }, options );
//
//             var shadow = $('<div></div>').css({
//                 position:    'absolute',
//                 top:         -10000,
//                 left:        -10000,
//                 width:       $self.width(),
//                 fontSize:    $self.css('fontSize'),
//                 fontFamily:  $self.css('fontFamily'),
//                 fontWeight:  $self.css('fontWeight'),
//                 lineHeight:  $self.css('lineHeight'),
//                 resize:      'none',
//                 'word-wrap': 'break-word'
//             }).appendTo(document.body);
//
//             var update = function(event)
//             {
//                 var times = function(string, number)
//                 {
//                     for (var i=0, r=''; i<number; i++) r += string;
//                     return r;
//                 };
//
//                 var val = self.value.replace(/&/g, '&amp;')
//                                     .replace(/</g, '&lt;')
//                                     .replace(/>/g, '&gt;')
//                                     .replace(/\n$/, '<br/>&#xa0;')
//                                     .replace(/\n/g, '<br/>')
//                                     .replace(/ {2,}/g, function(space){ return times('&#xa0;', space.length - 1) + ' ' });
//
//                 // Did enter get pressed?  Resize in this keydown event so that the flicker doesn't occur.
//                 if (event && event.data && event.data.event === 'keydown' && event.keyCode === 13) {
//                     val += '<br />';
//                 }
//
//                 shadow.css('width', $self.width());
//                 shadow.html(val + (noFlickerPad === 0 ? '...' : '')); // Append '...' to resize pre-emptively.
//
//                 var newHeight=Math.max(shadow.height() + noFlickerPad, minHeight);
//                 if(settings.preGrowCallback!=null){
//                   newHeight=settings.preGrowCallback($self,shadow,newHeight,minHeight);
//                 }
//
//                 $self.height(newHeight);
//
//                 if(settings.postGrowCallback!=null){
//                   settings.postGrowCallback($self);
//                 }
//             }
//
//             $self.change(update).keyup(update).keydown({event:'keydown'},update);
//             $(window).resize(update);
//
//             update();
//         });
//     };
// })(jQuery);
//
// // slick slide same height
//     var stHeight = $('#teachers .slick-track').height();
//     $('#teachers .slick-slide').css('height',stHeight + 'px' );
//
//     var stHeight = $('#feedbacks .slick-track').height();
//     $('#feedbacks .slick-slide').css('height',stHeight + 'px' );
//
//

//
//
// //equal hieght
//     ;( function( $, window, document, undefined )
//     {
//         'use strict';
//
//         var $list       = $( '.achievement' ),
//             $items      = $list.find( '.achievement__item' ),
//             setHeights  = function()
//             {
//                 $items.css( 'height', 'auto' );
//
//                 var perRow = Math.floor( $list.width() / $items.width() );
//                 if( perRow == null || perRow < 2 ) return true;
//
//                 for( var i = 0, j = $items.length; i < j; i += perRow )
//                 {
//                     var maxHeight   = 0,
//                         $row        = $items.slice( i, i + perRow );
//
//                     $row.each( function()
//                     {
//                         var itemHeight = parseInt( $( this ).outerHeight() );
//                         if ( itemHeight > maxHeight ) maxHeight = itemHeight;
//                     });
//                     $row.css( 'height', maxHeight );
//                 }
//             };
//
//         setHeights();
//         $( window ).on( 'resize', setHeights );
//         $list.find( 'img' ).on( 'load', setHeights );
//     })( jQuery, window, document );
//
//
//
//
//
// //form validation
//     // send form
//     $('#messageForm1 .butn, #messageForm2 .butn, #messageForm3 .butn').click(function () {
//         var parentClass = $(this).attr('rel');
//         validate = 1;
//         validate_msg = '';
//         form = $('#' + $(this).attr('data-rel'));
//         jQuery.each(form.find('.form-group input'), function (key, value) {
//
//             if ($(this).val() == '') {
//                 validate_msg += $(this).attr('title') + '\n';
//                 validate = 0;
//                 $(this).focus();
//
//                 $(this).addClass('red_input');
//
//                 $(this).keyup(function () {
//
//                     $(this).removeClass('red_input');
//
//                 });
//             }
//
//             else {
//                 $(this).removeClass('red_input');
//             }
//         });
//
//         if (validate == 1) {
//             $.ajax({
//                 url: 'send.php'
//                 , data: 'action=send_form&' + form.serialize()
//                 , success: function (data) {
//                     $('form').trigger('reset');
//                     // $('#call-back, #request').modal('hide');
//                     $('#thanks').modal('show');
//                 }
//             });
//         }
//         else {}
//     });
//
//     $(function(){
//
//     $(document).click(function (e) {
//         var div = $('._social ._link'),
//             div2 = $('._social ._all_link');
//         if ( !div.is(e.target) && div.has(e.target).length === 0 ) {
//             div2.hide();
//         }
//     });
//
// });
//
//
//
//
//
//


});
